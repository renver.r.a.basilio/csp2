<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return redirect('/menu');
});


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/menu', 'AssetController@index');
Route::get('/users/index', 'UserController@index');
Route::get('/borrowrequests/{id}/index', 'BorrowRequestController@index');
Route::get('/borrowrequests/admin', 'BorrowRequestController@adminrequest');

Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/users/create', 'UserController@create');
Route::post('/users/store', 'UserController@store');
Route::get('/users/{id}/edit', 'UserController@edit');
Route::put('/users/{id}', 'UserController@update');
Route::get('/users/{id}/delete-confirm', 'UserController@deleteConfirm');
Route::delete('/users/{id}', 'UserController@destroy');
Route::get('/users/history', 'UserController@userhistory');

Route::get('/assets/create', 'AssetController@create');
Route::post('/assets/store', 'AssetController@store');
Route::get('/assets/{id}/edit', 'AssetController@edit');
Route::put('/assets/{id}', 'AssetController@update');
Route::get('/assets/{id}/delete-confirm', 'AssetController@deleteConfirm');
Route::delete('/assets/{id}', 'AssetController@destroy');

Route::put('/borrowrequests/{id}', 'BorrowRequestController@store');
Route::get('/borrowrequests/{id}/approved', 'BorrowRequestController@approved');
Route::get('/borrowrequests/{id}/denied', 'BorrowRequestController@denied');
Route::get('/borrowrequests/{id}/returned', 'BorrowRequestController@returned');









