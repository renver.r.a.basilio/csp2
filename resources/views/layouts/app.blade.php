<!DOCTYPE html>

<html>

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>@yield('title')</title>

		<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
		<link href="{{ asset('css/mdb.min.css') }}" rel="stylesheet">
		<link href="{{ asset('css/style.css') }}" rel="stylesheet">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">

	</head>

	<body style="background: url(https://images.cdn4.stockunlimited.net/preview1300/musical-instrument-background_1403394.jpg); background-repeat: no-repeat; background-size: cover; background-attachment: fixed">

		@include('layouts.header')

		<main class="py-3" >
			@yield('content')
		</main>

		@include('layouts.footer')

		<script src="{{ asset('js/jquery.slim.min.js') }}" defer></script>
		<script src="{{ asset('js/popper.min.js') }}" defer></script>
		<script src="{{ asset('js/bootstrap.min.js') }}" defer></script>

	</body>

</html>