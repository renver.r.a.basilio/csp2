    <nav class="navbar navbar-expand-md navbar-dark shadow-sm sticky-top scrolling-navbar purple-gradient">
        <div class="container">

            <a class="navbar-brand" href="">{{ config('app_name', 'Musika') }} <i class="fas fa-guitar"></i></a>
            
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse text-right" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">

                    <a class="navbar-brand" href="{{ url('/menu') }}">
                        Menu
                    </a>

                    @if (!empty(Auth::user()) && Auth::user()->user_role == 'admin')

                        <a class="navbar-brand" href="{{ url('users/index') }}">Users</a>
                        <a class="navbar-brand" href="{{ url('borrowrequests/admin') }}">Requests</a>

                    @elseif (!empty(Auth::user()) && Auth::user()->user_role == 'student')

                        <a class="navbar-brand" href="{{ url('users/history') }}">History</a>

                    @endif

                    <!-- Authentication Links -->
                    @guest
                        
                        <a class="navbar-brand" href="{{ route('login') }}">{{ __('Login') }}</a>
                        <a class="navbar-brand" href="{{ route('register') }}">{{ __('Register') }}</a>
                        
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right deep-purple lighten-2" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>