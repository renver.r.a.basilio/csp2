@extends('layouts.app')

@section('title', 'Borrow Asset')

@section('borrow-asset-form')

<form action='{{ url("borrowrequests/$asset->id") }}' method="post" enctype="multipart/form-data">

		@csrf

		@method("PUT")

	    	<img src='{{ asset("storage/$asset->image_location") }}' class="card-img-top">

		    <div class="card-body">

		      	<h5 class="card-title">{{ $asset->name }}</h5>

		      	<p class="card-text">{{ $asset->description }}</p>

		      	<p class="card-text">{{ $asset->category->name }}</p>

		    </div>

		    <div class="card-footer">

    			<form action='{{ url("borrowrequests/$asset->id/index") }}' class="form-add-to-cart" data-id="{{ $asset->id }}">
    				<div class="btn-group btn-block">
    					<button class="btn btn-success">Borrow</button>
    				</div>
    			</form>

		    </div>

@endsection	

@section('content')

	<div class="container-fluid">

		<div class="row">

			<div class="col-6 mx-auto">

				<h3 class="text-center text-white">Borrow Instrument</h3>

				<div class="card">

					<div class="card-header">Instrument Information</div>

					<div class="card-body">

						@yield('borrow-asset-form')

					</div>

				</div>

			</div>

		</div>

	</div>

@endsection