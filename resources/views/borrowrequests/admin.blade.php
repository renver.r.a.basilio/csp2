@extends('layouts.app')

@section('title', 'BorrowRequest')

@section('content')

		<div class="bs-example">

		    <table class="table table-striped table-dark">

		        <thead>
		            <tr>
			            <th>ID</th>
		                <th>User Name</th>
		                <th>Instrument Name</th>
		                <th>Remarks</th>
		                <th>Status</th>
			            <th class="text-center">Admin</th>      
		            </tr>
		        </thead>

		        <tbody>

		        	@foreach ($borrowrequests as $borrowrequest)

			            <tr>
			                <td>{{ $borrowrequest->id }}</td>
			                <td>{{ $borrowrequest->user->name }}</td>
			                <td>{{ $borrowrequest->asset->name }}</td>
			                <td><textarea class="stylish-color" style="resize: none">{{ $borrowrequest->remarks }}</textarea></td>

			                @if($borrowrequest->borrow_status == 'Approved')
			                	<td><a class="font-weight-bold text-success">Approved</td>
			                @elseif($borrowrequest->borrow_status == 'Denied')
			                	<td><a class="font-weight-bold text-danger">Denied</a>
			                @elseif($borrowrequest->borrow_status == 'Pending')
			                	<td><a class="font-weight-bold text-warning">Pending</a></td>
			                @endif

			                @if($borrowrequest->borrow_status == 'Pending')

				                	<td class="btn-group btn-block">
				                		<a  class="btn btn-outline-info" href="/borrowrequests/{{$borrowrequest->id}}/approved">Approve<i class="fas fa-user-edit"></i></a>
				                		<a  class="btn btn-outline-danger ml-1" href="/borrowrequests/{{$borrowrequest->id}}/denied">Deny <i class="fas fa-user-times"></i></button>
				                	</td>
				            @elseif($borrowrequest->borrow_status == 'Approved' OR $borrowrequest->borrow_status == 'Denied')

				            		<td><p class="font-weight-bold text-success text-center">Processed</p></td>
				            @endif
	 			       	</tr>

				       	@endforeach

	       		</tbody>

			</table>

				<h3>History</h3>

				    <table class="table table-striped table-dark">

				        <thead>
				            <tr>
				                <th>User Name</th>
				                <th>Borrowed Instrument</th>
				                <th>Borrow Date</th>
				                <th>Returned Date</th>
				                <td>Tag as Returned</th>
				                <th>Remarks</th>
				                <th>Status</th>     
				            </tr>
				        </thead>

				        <tbody>

				        	@foreach ($histories as $history)

					            <tr>
					                
					                <td>{{ $history->user->name }}</td>
					                <td>{{ $history->asset->name }}</td>
					                <td>{{ $history->borrow_date }}</td>
					                <td>{{ $history->return_date }}</td>
					                @if($history->is_returned == 1)
					                	<td>Processed</td>
					                @elseif($history->is_borrowed == 1)
									<td><button><a href="/borrowrequests/{{ $history->id }}/returned">Returned</button></td>
									@endif
					                <td>remarks</td>
					                <td class="font-weight-bold text-warning">
					                	@if($history->is_borrowed == 1)
					                	<p class="text-success">Borrowed</p>
					                	@elseif($history->is_returned == 1 )
					                	<p class="text-success">Returned</p>
										@endif

					                </td>

			 			       	</tr>

						       	@endforeach

			       		</tbody>

					</table>


		</div>

@endsection

@if (!empty(session()->get('message')))
	<script>alert('{{ session()->get("message") }}')</script>
@endif