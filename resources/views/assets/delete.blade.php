@if (Auth::user()->user_role != 'admin')
	<script>window.location = '/menu'</script>
@endif

@extends('layouts.app')

@section('title', 'Edit Asset')

@section('delete-asset-form')

<form action='{{ url("assets/$asset->id") }}' method="post" enctype="multipart/form-data">

		@csrf

		@method("DELETE")

		<div class="form-group">
			<label>Asset Name</label>
			<input type="text" class="form-control" value="{{ $asset->name }}" readonly>
		</div>

		<div class="form-group">
			<label>Description</label>
			<input type="text" class="form-control" value="{{ $asset->description }}" readonly>
		</div>

		<div class="form-group">
			<label>Category</label>
			<input type="text" class="form-control" value="{{ $category->name }}" readonly>
		</div>

		<button type="submit" class="btn btn-danger btn-block">Delete</button>

	</form>

@endsection	

@section('content')

	<div class="container-fluid">

		<div class="row">

			<div class="col-6 mx-auto">

				<h3 class="text-center text-white">Edit Asset</h3>

				<div class="card">

					<div class="card-header">Asset Information</div>

					<div class="card-body">

						@yield('delete-asset-form')

					</div>

				</div>

			</div>

		</div>

	</div>

@endsection