@if (Auth::user()->user_role != 'admin')
	<script>window.location = '/menu'</script>
@endif

@extends('layouts.app')

@section('title', 'Add Asset')

@section('add-asset-form')

	<form action="{{ url('assets/store') }}" method="post" enctype="multipart/form-data">

		@csrf

		<div class="form-group">
			<label>Asset Name</label>
			<input type="text" class="form-control" name="name" required>
		</div>

		<div class="form-group">
			<label>Description</label>
			<input type="text" class="form-control" name="description">
		</div>

		<div class="form-group">
			<label>Category</label>
			<select class="form-control" name="category_id">
				<option value selected disabled>Select Category</option>
				@foreach ($categories as $category)
					<option value="{{ $category->id }}">{{ $category->name }}</option>
				@endforeach
			</select>
		</div>

		<div class="form-group">
			<label>Image</label>
			<input type="file" class="form-control" name="image" required>
		</div>

		<button type="submit" class="btn btn-success btn-block">Add</button>

	</form>

@endsection	

@section('content')

	<div class="container-fluid">

		<div class="row">

			<div class="col-6 mx-auto">

				<h3 class="text-center text-white">Add Asset</h3>

				<div class="card">

					<div class="card-header">Asset Information</div>

					<div class="card-body">

						@yield('add-asset-form')

					</div>

				</div>

			</div>

		</div>

	</div>

@endsection