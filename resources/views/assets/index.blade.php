@extends('layouts.app')

@section('title', 'Menu')

@section('content')

	<div class="containter-fluid">

		<h3 class="text-center text-white">Musical Instruments</h3>

		@if (!empty(Auth::user()) && Auth::user()->user_role == 'admin')
			<a href="{{ url('assets/create') }}" class="ml-3 btn purple-gradient">Add Item <i class="fas fa-plus"></i></a>
		@endif

		<div class="row mx-auto">

			@foreach ($assets as $asset)

			@if ($asset->is_available == 1)

				<div class="col-3 mt-3">

		 		 	<div class="card">

				    	<img src='{{ asset("storage/$asset->image_location") }}' class="card-img-top">
						<!-- <img src='{{ asset("$asset->image_location") }}' class="card-img-top"> 000webhost -->

					    <div class="card-body">

					      	<h5 class="card-title">{{ $asset->name }}</h5>

					      	<p class="card-text">{{ $asset->description }}</p>

					      	<p class="card-text">{{ $asset->category->name }}</p>

					    </div>

					    <div class="card-footer">

					    	@if (!empty(Auth::user()))

					    		@if (Auth::user()->user_role == "admin")

					    			<div class="btn-group">
					    				<a class="btn btn-outline-info" href='{{ url("assets/$asset->id/edit") }}'>Edit</a>
					    				<a class="btn btn-outline-danger" href='{{ url("assets/$asset->id/delete-confirm") }}'>Delete</a>
					    			</div>

					    		@elseif (Auth::user()->user_role == "student")

					    			<form action='{{ url("borrowrequests/$asset->id/index") }}' class="form-add-to-cart" data-id="{{ $asset->id }}">
					    				<div class="btn-group btn-block">
					    					<button class="btn btn-success">Borrow</button>
					    				</div>
					    			</form>

					    		@endif

					    	@endif

					    </div>

				  	</div>

				</div>

			@endif

		  	@endforeach

		</div>

	</div>

@endsection

@if (!empty(session()->get('message')))
	<script>alert('{{ session()->get("message") }}')</script>
@endif