@if (Auth::user()->user_role != 'admin')
	<script>window.location = '/menu'</script>
@endif

@extends('layouts.app')

@section('title', 'Edit Asset')

@section('edit-asset-form')

<form action='{{ url("assets/$asset->id") }}' method="post" enctype="multipart/form-data">

		@csrf

		@method("PUT")

		<div class="form-group">
			<label>Asset Name</label>
			<input type="text" class="form-control" value="{{ $asset->name }}" name="name">
		</div>

		<div class="form-group">
			<label>Description</label>
			<input type="text" class="form-control" value="{{ $asset->description }}" name="description">
		</div>

		<div class="form-group">
			<label>Category</label>
			<select class="form-control" name="category_id">
				<option value selected disabled>Select Category</option>
				@foreach ($categories as $category)
					@if ($category->id == $asset->category_id)
						<option value="{{ $category->id }}" selected>{{ $category->name }}</option>
					@else
						<option value="{{ $category->id }}">{{ $category->name }}</option>
					@endif
				@endforeach
			</select>
		</div>

		<div class="form-group">
			<label>Image</label>
			<input type="file" class="form-control" name="image">
		</div>

		<button type="submit" class="btn btn-success btn-block">Edit</button>

	</form>

@endsection	

@section('content')

	<div class="container-fluid">

		<div class="row">

			<div class="col-6 mx-auto">

				<h3 class="text-center text-white">Edit Asset</h3>

				<div class="card">

					<div class="card-header">Asset Information</div>

					<div class="card-body">

						@yield('edit-asset-form')

					</div>

				</div>

			</div>

		</div>

	</div>

@endsection
