@extends('layouts.app')

@section('title', 'Users')

@section('content')

	@if (!empty(Auth::user()) && Auth::user()->user_role == 'admin')

		<a href="{{ url('users/create') }}" class="ml-3 btn purple-gradient">Add User <i class="fas fa-plus"></i></a>

	@endif

	<div class="bs-example">

		    <table class="table table-striped table-dark">

		        <thead>
		            <tr>
		                <th>User ID</th>
		                <th>Name</th>
  		                <th>Email</th>
  		                <th>Address</th>
  		                <th>Contact</th>
  		                <th>User Role</th>
  		                <th>Admin</th>
		            </tr>
		        </thead>

		        <tbody>

		        	@foreach ($users as $user)

			            <tr>
			                <td>{{ $user->id }}</td>
			                <td>{{ $user->name }}</td>
			                <td>{{ $user->email }}</td>
			                <td>{{ $user->address }}</td>
			                <td>{{ $user->contact }}</td>
			                <td>{{ $user->user_role }}</td>
			                @if (!empty(Auth::user()) && Auth::user()->user_role == 'admin')
			                	<td class="btn-group btn-block">
			                		<a class="btn btn-outline-info" href='{{ url("users/$user->id/edit") }}'>Edit <i class="fas fa-user-edit"></i></a>
			                		<a class="btn btn-outline-danger ml-1" href='{{ url("users/$user->id/delete-confirm") }}'>Delete <i class="fas fa-user-times"></i></a>
			                	</td>
			                @endif
	 			       	</tr>

 			       	@endforeach

	       		</tbody>

		    </table>

	</div>

@endsection

@if (!empty(session()->get('message')))
	<script>alert('{{ session()->get("message") }}')</script>
@endif