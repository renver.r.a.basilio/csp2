@if (Auth::user()->user_role != 'admin')
	<script>window.location = '/menu'</script>
@endif

@extends('layouts.app')

@section('title', 'Delete User')

@section('delete-user-form')

<form action='{{ url("users/$user->id") }}' method="post" enctype="multipart/form-data">

		@csrf

		@method("DELETE")

		<div class="form-group">
			<label>Name</label>
			<input type="text" class="form-control" value="{{ $user->name }}" name="name" readonly>
		</div>

		<div class="form-group">
			<label>Email</label>
			<input type="text" class="form-control" value="{{ $user->email }}" name="email" readonly>
		</div>

		<div class="form-group">
			<label>Address</label>
				<input type="text" class="form-control" value="{{ $user->address }}" name="address" readonly>
		</div>

		<div class="form-group">
			<label>Contact</label>
			<input type="text" class="form-control" value="{{ $user->contact }}" name="contact" readonly>
		</div>

		<div class="form-group">
			<label>User Role</label>
			<input type="text" class="form-control" value="{{ $user->user_role }}" name="user_role" readonly>
		</div>

		<button type="submit" class="btn btn-danger btn-block">Delete</button>

	</form>

@endsection	

@section('content')

	<div class="container-fluid">

		<div class="row">

			<div class="col-6 mx-auto">

				<h3 class="text-center text-white">Edit Asset</h3>

				<div class="card">

					<div class="card-header">Asset Information</div>

					<div class="card-body">

						@yield('delete-user-form')

					</div>

				</div>

			</div>

		</div>

	</div>

@endsection