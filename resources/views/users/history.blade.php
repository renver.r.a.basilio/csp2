@extends('layouts.app')

@section('title', 'User History')

@section('content')


	<h3 class="text-center text-white">History</h3>

		<div class="bs-example">

			<div class="row mx-auto justify-content-center">

			    <table class="col-6 table table-striped table-dark">

			        <thead>
			            <tr>
			                <th>Name</th>
	  		                <th>Email</th>
	  		                <th>Instrument</th>
	  		                <th>Remarks</th>
	  		                <th>Status</th>
	  		            </tr>
			        </thead>

			        <tbody>

			        	@foreach ($borrowrequests as $borrowrequest)
			        
				            <tr style="height: 100px">
				                <td>{{ $borrowrequest->user->name }}</td>
				                <td>{{ $borrowrequest->user->email }}</td>
				                <td>{{ $borrowrequest->asset->name }}</td>
				                <td>{{ $borrowrequest->remarks }}</td>
				                <td>{{ $borrowrequest->borrow_status }}</td>
				            </tr>
				        @endforeach

			    	</tbody>

			    </table>

			    <table class="col-6 table table-striped table-dark">

			        <thead>
			            <tr>
			                <th>Instrument</th>
			                <th>Borrow Date</th>
			                <th>Return Date</th>
			                <th>Status</th>
			            </tr>
			        </thead>

			        <tbody>

			        	@foreach ($histories as $history)
			        
				            <tr style="height: 100px">
				                <td>{{ $history->asset->name }}</td>
				                <td>{{ $history->borrow_date }}</td>
				                <td>{{ $history->return_date }}</td>
				                <td class="font-weight-bold text-warning">
				                	@if($history->is_borrowed == 1)
				                	<p class="text-warning">Borrowed</p>
				                	@elseif($history->is_returned == 1 )
				                	<p class="text-success">Returned</p>
				                	@endif
							</tr>
				        @endforeach

				  	</tbody>

			    </table>

			</div>

		</div>

@endsection