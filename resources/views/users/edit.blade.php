@if (Auth::user()->user_role != 'admin')
	<script>window.location = '/menu'</script>
@endif

@extends('layouts.app')

@section('title', 'Edit Asset')

@section('edit-asset-form')

<form action='{{ url("users/$user->id") }}' method="post" enctype="multipart/form-data">

		@csrf

		@method("PUT")

		<div class="form-group">
			<label>Name</label>
			<input type="text" class="form-control" value="{{ $user->name }}" name="name" required>
		</div>

		<div class="form-group">
			<label>Email</label>
			<input type="text" class="form-control" value="{{ $user->email }}" name="email">
		</div>

		<div class="form-group">
			<label>Address</label>
				<input type="text" class="form-control" value="{{ $user->address }}" name="address">
		</div>

		<div class="form-group">
			<label>Contact</label>
			<input type="text" class="form-control" value="{{ $user->contact }}" name="contact">
		</div>

		<div class="form-group">
			<label>User Role</label>
			<input type="text" class="form-control" value="{{ $user->user_role }}" name="user_role">
		</div>

		<button type="submit" class="btn btn-success btn-block">Edit</button>

	</form>

@endsection	

@section('content')

	<div class="container-fluid">

		<div class="row">

			<div class="col-6 mx-auto">

				<h3 class="text-center text-white">Edit User</h3>

				<div class="card">

					<div class="card-header">User Information</div>

					<div class="card-body">

						@yield('edit-asset-form')

					</div>

				</div>

			</div>

		</div>

	</div>

@endsection