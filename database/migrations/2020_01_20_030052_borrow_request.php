<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BorrowRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('borrowrequests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('remarks', 100)->nullable();
            $table->string('borrow_status', 50)->nullable();
            $table->boolean('status')->nullable();
            $table->dateTime('returned_date');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('asset_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('asset_id')->references('id')->on('assets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('borrowrequests');
    }
}
