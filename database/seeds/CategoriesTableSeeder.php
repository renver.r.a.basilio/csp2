<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create(['name' => 'String']);
        Category::create(['name' => 'Woodwind']);
        Category::create(['name' => 'Brass']);
        Category::create(['name' => 'Percussion']);
        Category::create(['name' => 'Keyboard']);
    }
}
