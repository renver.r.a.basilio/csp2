<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowRequest extends Model
{

	protected $table = 'borrowrequests';

    public function user() {
    	return $this->belongsTo("App\User");
    }

    public function asset() {
    	return $this->belongsTo("App\Asset");
    }
    protected $fillable = ['status', 'borrow_status'];

}
