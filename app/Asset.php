<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    public $timestamps = true;
    
    public function category() {
    	return $this->belongsTo('App\Category', 'category_id');
	}
}
