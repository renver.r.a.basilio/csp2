<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Category;
use App\Asset;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assets = Asset::where('is_deleted', 0)->get();
        return view('assets.index', [
            'assets' => $assets
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoryOptions = Category::all();
        return view('assets.create', [
            'categories' => $categoryOptions
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $asset = new Asset;

        $asset->name = $request->name;
        $asset->description = $request->description;
        $asset->category_id = $request->category_id;
        $asset->is_available = 1;

        // dd($request->all());
        // $path = $request->image->store('images', ['disk' => 'public']); 000webhost
        $path = $request->image->store('images', 'public');
        $asset->image_location = $path;

        $asset->save();
        $request->session()->flash('message', 'The asset has been added!');

        return redirect('/menu');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asset = Asset::find($id);
        $categories = Category::all();
        return view('assets.edit', [
            'asset' => $asset,
            'categories' => $categories
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $asset = Asset::find($id);

        $asset->name = $request->name;
        $asset->description = $request->description;
        $asset->category_id = $request->category_id;

        if ($request->hasFile('image')) {
            //remove the old file.
            Storage::disk('public')->delete($asset->image_location);
            //save the new image.
            $path = $request->image->store('images', 'public');
            $asset->image_location = $path;
        }

        $asset->save();
        $request->session()->flash('message', 'The asset has been updated.');

        return redirect('/menu');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $asset = Asset::find($id);

        $asset->is_deleted = 1;
        $asset->save();

        $request->session()->flash('message', 'The asset has been deleted.');

        return redirect('/menu');
    }

    public function deleteConfirm($id)
    {
        $asset = Asset::find($id);
        $category = Category::find($asset->category_id);
        return view('assets.delete', [
            'asset' => $asset,
            'category' => $category
        ]);
    }
}
