<?php

namespace App\Http\Controllers;

use Illuminate\Suppoer\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\BorrowRequest;
use App\History;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('is_deleted', 0)->get();
        return view('users.index', [
            'users' => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.register');
    }

    public function userhistory()
    {
        $borrowrequest = BorrowRequest::where('user_id', Auth::user()->id)->get();
        $histories = History::where('user_id', Auth::user()->id)->get();
        return view('users.history', [
            'borrowrequests' => $borrowrequest,
            'histories' => $histories
        ]);
    }   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'password' => 'required|min:8|confirmed',
        ]);

        $user = new User;

        $user->name = $request->name;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->contact = $request->contact;
        $user->password = Hash::make($request->password);
        $user->user_role = 'student';

        $user->save();
        $request->session()->flash('message', 'The user has been added!');

        return redirect('users/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('users.edit', [
            'user' => $user
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->contact = $request->contact;
        $user->user_role = $request->user_role;

        $user->save();
        $request->session()->flash('message', 'The user has been updated!');

        return redirect('users/index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = User::find($id);

        $user->is_deleted = 1;
        $user->save();

        $request->session()->flash('message', 'The user has been deleted.');

        return redirect('users/index');
    }

    public function deleteConfirm($id)
    {
        $user = User::find($id);
        return view('users.delete', [
            'user' => $user
        ]);
    }
}
