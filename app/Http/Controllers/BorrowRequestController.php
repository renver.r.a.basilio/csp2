<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\BorrowRequest;
use App\Asset;
use App\History;

class BorrowRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $asset = Asset::find($id);
        return view('borrowrequests.index', [
            'asset' => $asset
        ]);
    }
    public function adminrequest()
    {
        $borrowrequest = BorrowRequest::all();
        $histories = History::all();
        return view('borrowrequests.admin', [
            'borrowrequests' => $borrowrequest,
            'histories' =>$histories
        ]);

    }
    
    public function approved(Request $request, $id)
    {
        $borrowrequests = BorrowRequest::find($id);

        $asset = Asset::find($borrowrequests->asset_id);
        $asset->is_available = 0;
        $asset->save();

        $timestamp = now();

        $history = new History;
        $history->is_borrowed = 1;
        $history->is_returned = 0;
        $history->asset_id = $borrowrequests->asset_id;
        $history->user_id = $borrowrequests->user_id;
        $history->borrow_date = $timestamp;
        $history->save();

        if($borrowrequests->status !== 1 ) {

            $status = 'Approved';
            $borrowrequests->update([
                'status' => 1,
                'borrow_status' => $status
            ]);

        }

        return back();
    }
    public function denied(Request $request, $id)
    {
        $borrowrequests = BorrowRequest::find($id);

        if($borrowrequests->status == 0 && $borrowrequests->borrow_status == 'Pending') {

            $status = 'Denied';
            $borrowrequests->update([
                'status' => 0,
                'borrow_status' => $status
            ]);

        }


        return back();
    }
    public function returned(Request $request, $id)
    {
        $timestamp = now();

        $history = History::find($id);
        $history->is_borrowed = 0;
        $history->is_returned = 1;
        $history->return_date = $timestamp;
        $history->save();

        $asset = Asset::find($history->asset_id);
        $asset->is_available = 1;
        $asset->save();

        return back();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $borrowrequest = new BorrowRequest;

        $borrowrequest->user_id = Auth::user()->id;
        $borrowrequest->asset_id = $id;
        $borrowrequest->borrow_status = 'Pending';

        $borrowrequest->save();
        $request->session()->flash('message', 'The asset is pending.');

        return redirect('/menu');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
